import mangaActions from "./constants";
import AsyncStorage from "@react-native-community/async-storage";

const initialState = async () => {
  let lasts = await AsyncStorage.getItem("lasts");

  if (lasts === null || lasts === undefined) {
    lasts = []
    await AsyncStorage.setItem("lasts", JSON.stringify(lasts))
  } else {
    lasts = JSON.parse(lasts);
  }

  return {
    lasts
  }
}

export default async function mangaReducer(state = initialState(), action) {
  switch (action.type) {
    case mangaActions.READ:
      return {
        ...state,
        lasts: action.lasts
      };
    default:
      return state;
  }
}