import mangaActions from "./constants";
import store from "../index";
import AsyncStorage from "@react-native-community/async-storage";

export async function read(name, image, chapter) {
  const currentStore = store.getState();

  let lasts = (await currentStore.mangas).lasts
  // Get only one element for each manga
  lasts = lasts.filter(last => last.name !== name);
  lasts.unshift({
    name,
    image,
    chapter
  });

  await AsyncStorage.setItem("lasts", JSON.stringify(lasts));

  return async function (dispatch) {
    dispatch({
      type: mangaActions.READ,
      lasts
    });
  };
}