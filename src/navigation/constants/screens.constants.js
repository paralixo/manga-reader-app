export const ScreensConstants = {
    HOME: "Home",
    LIST: "Your last reads",
    SEARCH: "Search",
    PROFILE: "Profile",
    MANGA: "Manga",
    READER: "Reader"
}