import React from "react";

import { StatusBar } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { connect } from "react-redux";
import StackComponent from "../components/stacks/Stacks"
import Home from "../screens/Home"
import List from "../screens/List"
import Search from "../screens/Search"
import Profile from "../screens/Profile"
import Manga from "../screens/Manga";
import Reader from "../screens/Reader";
import {ScreensConstants} from "./constants/screens.constants";

const Drawer = createDrawerNavigator();

const MainNavigation = (toto) => {
  const { user } = toto;

  return (
    <NavigationContainer>
      <StatusBar
        hidden={false}
        backgroundColor="transparent"
        barStyle="dark-content"
      />
      <Drawer.Navigator initialRouteName={ScreensConstants.HOME}>
        <Drawer.Screen name={ScreensConstants.HOME}>
          {() => <StackComponent user={user} routeName={ScreensConstants.HOME} screen={Home} headerText={ScreensConstants.HOME}/>}
        </Drawer.Screen>
        <Drawer.Screen name={ScreensConstants.LIST}>
          {() => <StackComponent user={user} routeName={ScreensConstants.LIST} screen={List} headerText={ScreensConstants.LIST}/>}
        </Drawer.Screen>
        <Drawer.Screen name={ScreensConstants.SEARCH}>
          {() => <StackComponent user={user} routeName={ScreensConstants.SEARCH} screen={Search} headerText={ScreensConstants.SEARCH}/>}
        </Drawer.Screen>
        <Drawer.Screen name={ScreensConstants.PROFILE}>
          {() => <StackComponent user={user} routeName={ScreensConstants.PROFILE} screen={Profile} headerText={ScreensConstants.PROFILE}/>}
        </Drawer.Screen>

        {/* TODO: réussir à ne pas afficher ces elements dans le menu (pas juste enlever le texte) */}
        <Drawer.Screen name={ScreensConstants.MANGA} options={{ drawerLabel: () => null }}>
          {() => <StackComponent user={user} routeName={ScreensConstants.MANGA} screen={Manga} headerText={ScreensConstants.MANGA}/>}
        </Drawer.Screen>
        <Drawer.Screen name={ScreensConstants.READER} options={{ drawerLabel: () => null }}>
          {() => <StackComponent user={user} routeName={ScreensConstants.READER} screen={Reader} headerText={ScreensConstants.READER}/>}
        </Drawer.Screen>
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(MainNavigation);
