import React from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
} from "react-native";
import {ScreensConstants} from "../../../navigation/constants/screens.constants";

export default class LastRead extends React.Component {
    constructor(props) {
        super(props);
        this.manga = props.manga;
    }

    goToReaderScreen() {
        this.props.navigation.navigate(ScreensConstants.READER, { screen: ScreensConstants.READER, params: {
            link: this.manga.chapter.link,
            mangaName: this.manga.name,
            mangaCover: this.manga.image
        }});
    }

    render(){
        return(
            <TouchableOpacity onPress={() => this.goToReaderScreen()} style={styles.items}>
                <Image source={{uri: this.manga.image}} style={styles.images}/>
                <Text style={styles.text}> {this.manga.name} - Chapter #{this.manga.chapter.number}</Text>
            </TouchableOpacity>
        );
    }

}

const styles = StyleSheet.create({
    items: {
        textDecorationLine: "none",
        marginBottom: 15,
        flexDirection: "row",
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "black",
    },
    images: {
        flex: 1,
        resizeMode: "cover",
        maxWidth: "20%",
    },
    text: {
        color: "black",
        fontSize: 20,
        fontWeight: "300",
        padding: "5%",
    },
});
