import React from "react";
import {
    View,
    Text,
    StyleSheet,
    FlatList
} from "react-native";
import MangaItem from "./items/MangaItem"
import Loader from "../Loader";
import LastRead from "./items/LastRead";


export default class MangaList extends React.Component {
    constructor(props) {
        super(props);
        this.lastReadMode = props.lastReadMode;
    }

    renderItem = (manga) => {
        if (this.lastReadMode) {
            return (
                <LastRead navigation={this.props.navigation} manga={manga.item}/>
            )
        }

        return (
            <MangaItem navigation={this.props.navigation} manga={manga.item}/>
        );
    }

    render(){
        return (
            <View style={styles.container}>
                <Text style={styles.title}>{this.props.title}</Text>

                {this.props.loading
                    ? <Loader/>
                    : <FlatList
                        data={this.props.mangas}
                        renderItem={(item) => this.renderItem(item)}
                        keyExtractor={(item, index) => index.toString()}
                        style={styles.list}
                    />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        paddingVertical: 4,
        margin: 5
    },
    title: {
        textAlign: "center",
        fontWeight: "500",
        color: "black",
        fontSize: 22
    }
});
