import React from "react";
import {ActivityIndicator, StyleSheet, View} from "react-native";

export default class Loader extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <View style={styles.loader}>
                <ActivityIndicator size="large" color="#0c9"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loader: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
    }
});