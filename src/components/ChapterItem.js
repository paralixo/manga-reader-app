import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import {ScreensConstants} from "../navigation/constants/screens.constants";
import {read} from "../redux/manga/actions";

export default class ChapterItem extends React.Component {
    constructor(props) {
        super(props);
        this.chapter = props.chapter;
        this.mangaName = props.mangaName;
        this.mangaCover = props.mangaCover;
    }

    async goToReaderScreen() {
        await read(this.mangaName, this.mangaCover, this.chapter);
        this.props.navigation.navigate(ScreensConstants.READER, { screen: ScreensConstants.READER, params: {
            link: this.chapter.link,
            mangaName: this.mangaName,
            mangaCover: this.mangaCover
        }});
    }

    render(){
        return(
            <TouchableOpacity onPress={() => {this.goToReaderScreen()}} style={styles.button}>
                <Text style={styles.chapterTitle}>#{this.chapter.number} - {this.chapter.name}</Text>
            </TouchableOpacity>
        );
    }

}

const styles = StyleSheet.create({
    chapterTitle: {
        fontSize: 14,
        color: "white",
        textAlign: "center",
    },
    button: {
        backgroundColor: "#24a0ed",
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        width: 220
      },
});
