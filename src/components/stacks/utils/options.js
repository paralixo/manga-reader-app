import {BurgerMenu} from "../BurgerMenu";
import {StyleSheet, Text} from "react-native";
import React from "react";

export default function getOptions(navigation, user, headerText) {
    return {
        headerTitle: null,
        headerLeft: () => (
            <BurgerMenu navigation={navigation} user={user}/>
        ),
        headerRight: () => (
            <Text style={styles.title}>{headerText}</Text>
        ),
        headerStyle: {
            backgroundColor: '#fff',
        },
        headerTintColor: '#000',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: "700",
        marginRight: 20,
    }
});