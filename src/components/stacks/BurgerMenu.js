import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React from "react";

export const BurgerMenu = (props) => {
    const { navigation, user } = props;
    const burgerIcon = "https://img.icons8.com/material/2x/menu.png";
    const colorStatus = user.loggedIn ? 'green' : 'red';

    function styleUserStatus () {
        return [styles.userStatus, { 'backgroundColor': colorStatus }]
    }

    return (
        <View style={[styles.headerLeft]}>
            <TouchableOpacity
                onPress={() => navigation.toggleDrawer()}
                style={styles.userHeader}
            >
                <Image
                    source={{ uri: burgerIcon}}
                    style={styles.logo}
                />
            </TouchableOpacity>

            <Text style={styles.userName}>{user.name}</Text>
            <View style={styleUserStatus()} />
        </View>
    )
}

const styles = StyleSheet.create({
    logo: {
        marginLeft: 20,
        width: 28,
        height: 28,
    },
    userHeader: {
        flexDirection: "row",
        alignItems: "center",
    },
    userName: {
        fontWeight: "600",
        marginLeft: 12,
    },
    userStatus: {
        width: 8,
        height: 8,
        borderRadius: 50,
        marginLeft: 4,
        marginTop: -14,
    },
    headerLeft: {
        flexDirection:'row',
        alignItems:'center',
    },
});