import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import options from "./utils/options.js";

const NewStack = createStackNavigator();

const StackComponent = (props) => {
  const { user, routeName, screen, headerText } = props;

  return (
    <NewStack.Navigator initialRouteName={routeName}>
      <NewStack.Screen
        name={routeName}
        component={screen}
        options={({ navigation }) => options(navigation, user, headerText)}
      />
    </NewStack.Navigator>
  );
}

export default StackComponent