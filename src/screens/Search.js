import React from 'react';
import {TouchableOpacity, Text, TextInput, View, StyleSheet} from 'react-native';
import MangaList from "../components/manga-list/MangaList";
import {API_HOST, MANGA_PATH, SEARCH_PARAM} from "../api/api.constants";

export default class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            mangas: [],
            searchInput: ""
        };
    }

    componentDidMount() {
        this.props.navigation.addListener('blur', () => {
            this.setState({
                searchInput: ""
            })
        });
    }

    search() {
        this.setState({
            loading: true
        })

        fetch(API_HOST + MANGA_PATH + SEARCH_PARAM + this.state.searchInput)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    loading: false,
                    mangas: responseJson,
                });
            })
    }

    updateTextInput(event) {
        this.setState({
            searchInput: event.nativeEvent.text
        })
    }

    render(){
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    value={this.state.searchInput}
                    onChange={(event) => this.updateTextInput(event)}
                    placeholder="Insert manga name..."
                />
                <TouchableOpacity onPress={() => this.search()} style={styles.button}>
                    <Text style={styles.buttonText}>Search</Text>
                </TouchableOpacity>

                <MangaList navigation={this.props.navigation} title="Results" mangas={this.state.mangas} loading={this.state.loading}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: "5%"
    },
    input: {
        margin: 15,
        marginTop: 5,
        marginBottom: 5,
        height: 40,
        borderColor: '#51bfa4',
        borderWidth: 1,
        padding: 5,
        borderRadius: 10
    },
    button: {
        backgroundColor: '#51bfa4',
        marginTop: 5,
        padding: 10,
        margin: 15,
        height: 40,
        borderRadius: 10
    },
    buttonText: {
        color: "white",
        textAlign: "center"
    }
});