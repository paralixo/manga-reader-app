import React from "react";
import {View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, ScrollView} from "react-native";
import {API_HOST} from "../api/api.constants";
import {read} from "../redux/manga/actions";
import Loader from "../components/Loader";

export default class Reader extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            page: {},
            number: this.getPageNumber(),
            chapter: this.getChapterNumber(),
        }
    }

    componentDidMount() {
        this.props.navigation.addListener('focus', () => {
            this.fetchPageData(this.props.route.params.link)
        })
        this.props.navigation.addListener('blur', () => {
            this.setState({
                loading: true,
                page: {}
            })
        })
    }

    getPageNumber(value=this.props.route.params.link){
        return value.split('/')[4]
    }

    getChapterNumber(value=this.props.route.params.link){
        return value.split('/')[3]
    }

    styleImage() {
        const screenWidth = Dimensions.get('window').width
        const scale = this.state.page.width/screenWidth
        const imageHeight = this.state.page.height/scale
        return {
            width: screenWidth,
            height: imageHeight
        }
    }

    fetchPageData(value){
        this.setState({
            loading: true
        })
        fetch(API_HOST + value)
        .then((response) => response.json())
        .then(async (responseJson) => {
            this.setState({
                loading: false,
                page: responseJson,
                number: this.getPageNumber(value),
                chapterNumber: this.getChapterNumber(value)
            });

            await read(this.props.route.params.mangaName, this.props.route.params.mangaCover, {link: value, number: this.state.chapterNumber});
        })
    }

    goToNextPage() {
        this.fetchPageData(this.state.page.next)
    }

    goToPreviousPage() {
        this.fetchPageData(this.state.page.previous)
    }

    render() {
        if (this.state.loading) {
            return (
                <Loader/>
            );
        }
        return (
            <ScrollView>
                <Text style={styles.pageNumber}>Chapter #{this.state.chapterNumber} - Page {this.state.number}</Text>
                <Image
                    style={
                        this.styleImage()
                    }
                    source={
                        {uri:this.state.page.image}
                    }
                />
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={[styles.buttonPrevious, styles.button]} onPress={()=>{this.goToPreviousPage()}}>
                        <Text style={styles.buttonText}>Previous</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.buttonNext, styles.button]} onPress={()=>{this.goToNextPage()}}>
                        <Text style={styles.buttonText}>Next</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    pageNumber: {
        textAlign: "center",
        backgroundColor: "#40b5f5",
        fontWeight: "bold",
        color: "white"
    },
    buttonContainer: {
        flexDirection: "row",
    },
    button: {
        padding: 20,
        marginVertical: 8,
        width: "40%",
        borderRadius: 10
    },
    buttonText: {
        fontWeight: "bold",
        color: "white",
        textAlign: "center",
    },
    buttonNext: {
        backgroundColor: "#51bfa4",
        marginLeft: "5%",
    },
    buttonPrevious: {
        backgroundColor: "#f55945",
        marginLeft: "7%",
    },
});