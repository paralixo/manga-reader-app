import React from "react";
import { connect } from "react-redux";
import {View, Text, FlatList, StyleSheet, Image, ScrollView} from "react-native";
import {API_HOST, MANGA_PATH} from "../api/api.constants";
import Loader from "../components/Loader";
import ChapterItem from "../components/ChapterItem"

class Manga extends React.Component {
    constructor(props){
        super(props);

        this.user=props.user
        this.state = {
            manga: {},
            loading: true,
        }
    }

    componentDidMount() {
        this.props.navigation.addListener('focus', () => {
            if (this.props.route.params !== null && this.props.route.params !== undefined) {
                fetch(API_HOST + MANGA_PATH + this.props.route.params.url)
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({
                            loading: false,
                            manga: responseJson,
                        });
                    })
            }
        });
        this.props.navigation.addListener('blur', () => {
            this.setState({
                loading: true,
                manga: {}
            })
        });
    }

    renderItem (chapter) {
        return (
          <ChapterItem
            chapter={chapter.item}
            navigation={this.props.navigation}
            mangaName={this.state.manga.title}
            mangaCover={this.state.manga.image}
          />
        );
    };

    render(){
        if (!this.user.loggedIn) {
            return (
              <View style={styles.wrapper}>
                <Text>You must be logged in in order to access this page.</Text>
              </View>
            );
        }

        if (this.state.loading) {
            return (
                <Loader/>
            );
        }
        return (
          <ScrollView>
            <View style={styles.container}>
              {/* Ce component affiche les données sur la même ligne */}
              <View style={styles.childContainer}>
              
                {/* Ce component affiche les données sur la même colonne */}
                <View style={styles.informations}>
                  <Text style={styles.mangaTitle}>{this.state.manga.title}</Text>
                  <View style={styles.descriptionContainer}>
                    <Text style={styles.label}>Description</Text>
                    <Text style={styles.descriptionContent}>
                      {this.state.manga.description}
                    </Text>
                    <View style={styles.labelContainer}>
                      <Text style={styles.label}>Author : </Text>
                      <Text style={styles.labelInfo}>{this.state.manga.artist}</Text>
                    </View>
                    <View style={styles.labelContainer}>
                      <Text style={styles.label}>Year : </Text>
                      <Text style={styles.labelInfo}>{this.state.manga.year}</Text>
                    </View>
                  </View>
                </View>
        
                <Image
                  source={{ uri: this.state.manga.image }}
                  style={styles.img}
                />
              </View>
        
              <View style={styles.separator}><Text>______________________________________________</Text></View>
              <Text style={styles.chaptersHeader}>All chapters</Text>
              <View style={styles.container}>
                <FlatList
                  data={this.state.manga.chapters}
                  renderItem={(item) => this.renderItem(item)}
                  keyExtractor={(item) => item.number.toString()}
                />
              </View>
            </View>
          </ScrollView>
        );    
    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  childContainer: {
    flexDirection: "row",
    marginTop: 30,
  },
  informations: {
    flexDirection: "column",
    marginRight: 25
  },
  mangaTitle:{
    fontWeight: "bold"
  },
  img: {
    width: 180, 
    height: 250
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor:'black',
  }, 
  descriptionContainer: {
    width: 150,
    marginTop: 15,
  },
  descriptionContent: {
    fontSize: 10,
    textAlign: "justify",
    marginBottom: 10,
  },
  separator: {
    marginTop: 20,
    marginBottom: 15
  },
  chaptersHeader: {
    fontWeight: "bold",
    fontSize: 18,
  },
  labelContainer: {
    marginBottom: 5,
    flexDirection: "row",
    width: 220
  },
  label: {
    fontWeight: "bold",
    fontSize: 12,
  },
  labelInfo: {
    fontSize: 12,
  },
});

const mapStateToProps = (state) => ({
    user: state.user,
  });
  
export default connect(mapStateToProps)(Manga);