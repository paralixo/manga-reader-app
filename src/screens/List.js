import React from "react";
import { connect } from "react-redux";
import {View, Text, StyleSheet} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import MangaList from "../components/manga-list/MangaList";
import Loader from "../components/Loader";

class List extends React.Component {
  constructor(props){
    super(props);
    this.user=props.user

    this.state = {
      mangas: {},
      loading: true,
    }
  }

  componentDidMount() {
    this.props.navigation.addListener('focus', async () => {
      this.setState({
        loading: false,
        mangas: JSON.parse(await AsyncStorage.getItem("lasts"))
      })
    });
    this.props.navigation.addListener('blur', () => {
      this.setState({
        loading: true
      })
    });
  }

  render(){
    if (!this.user.loggedIn) {
      return (
          <View style={styles.wrapper}>
            <Text>You must be logged in in order to access this page.</Text>
          </View>
      );
    }

    if (this.state.loading) {
      return (
          <Loader/>
      );
    }

    return (
        <View style={styles.container}>
          <MangaList navigation={this.props.navigation} title="Your last reads" mangas={this.state.mangas} lastReadMode={true}/>
        </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: "5%"
  }
});

const mapStateToProps = (state) => ({
  user: state.user,
  mangas: state.mangas
});

export default connect(mapStateToProps)(List);