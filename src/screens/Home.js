import React from "react";
import {View, StyleSheet, Text} from "react-native";
import MangaList from "../components/manga-list/MangaList"
import { API_HOST, MANGA_PATH, SEARCH_PARAM} from "../api/api.constants"
import Loader from "../components/Loader";
import AsyncStorage from "@react-native-community/async-storage";
import LastRead from "../components/manga-list/items/LastRead";

class Home extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      mangas: [],
      loading: true,
      lastManga: []
    }
  }

  componentDidMount() {
    // On souhaite rafraichir la dernière lecture (uniquement) à chaque focus
    this.props.navigation.addListener('focus', async () => {
      this.setState({loading: true})
      let lastManga = JSON.parse(await AsyncStorage.getItem("lasts"));
      this.setState({
        loading: false,
        lastManga: lastManga.length === 0 ? undefined : lastManga[0]
      });
    });

    fetch(API_HOST + MANGA_PATH + SEARCH_PARAM)
    .then((response) => response.json())
    .then(async (responseJson) => {
      this.setState({
          loading: false,
          mangas: responseJson
      });
    })
  }

  render(){
    if (this.state.loading) {
      return (
          <Loader/>
      );
    }
    return (
      <View style={styles.container}>
        { this.state.lastManga && (
          <View>
            <Text style={styles.title}>Your last read</Text>
            <LastRead navigation={this.props.navigation} manga={this.state.lastManga}/>
          </View>
        )}
        <MangaList navigation={this.props.navigation} title="All mangas" mangas={this.state.mangas}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: "5%"
  },
  title: {
    textAlign: "center",
    fontWeight: "500",
    color: "black",
    fontSize: 22
  }
});

export default Home;
