# Manga Reader
> Par Lafuente Florian, Seghir Souleimane et Galmot Benoit

Manga Reader est une application mobile permettant de retrouver un manga et de le lire facilement depuis son smartphone.

## Installation
Installer `expo` globalement
```
npm install -g expo-cli
```

Cloner le répertoire
```
git clone git@gitlab.com:paralixo/manga-reader-app.git

cd manga-reader-api
```
Installer les dépendances
```
npm install
```

## Lancement
Pour avoir accès à l'ensemble des fonctionnalités il est nécessaire d'avoir accès à notre API (lien du repo en bas du README).
Vous pouvez tester le bon fonctionnement de l'API avec cette url :
http://florianl.eu-4.evennode.com/mangas/naruto/

Ensuite, dans votre terminal lancer au choix : 
```
npm start
expo start
```

Vous allez être redirigé vers http://localhost:19002, depuis cette interface vous pourrez scanner le QR code pour utiliser l'application (ou lancer un émulateur si vous en possèdez un).

## Fonctionnalités
1. Accéder à une liste de mangas

![](https://i.imgur.com/AZM2FxQ.png)

2. Rechercher un manga spécifique

![](https://i.imgur.com/pon8wly.png)
![](https://i.imgur.com/SomUDiT.png)


3. Obtenir des informations sur un manga et voir ses chapitres

![](https://i.imgur.com/d6G3I4P.png)
![](https://i.imgur.com/GeUPRcy.png)


4. Accéder aux derniers chapitres lus

![](https://i.imgur.com/EiVT1b4.png)


5. Lire les différents chapitres d'un manga

![](https://i.imgur.com/Y1GNXbX.png)
![](https://i.imgur.com/hRDhQuD.png)


---
### Manga Reader API
> https://gitlab.com/paralixo/manga-reader-api